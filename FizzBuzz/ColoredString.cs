﻿using System;

namespace FizzBuzz
{
    public class ColoredString
    {
        public ConsoleColor Color;                                                    
        public string Content;                                                         

        public ColoredString(string content, ConsoleColor color = ConsoleColor.White)
        {
            Color = color;
            Content = content;
        }
    }
}
