﻿namespace FizzBuzz
{
    public class NestedDirectories
    {
        public string[] DirectoriesPaths;               
        public bool[] FilledWithEmptyFile;              
        public int Size;                                

        public NestedDirectories(int size)
        {
            Size = size;
            DirectoriesPaths = new string[5];
            FilledWithEmptyFile = new[] { false, false, false, false, false };
        }
    }
}
