﻿using System;
using System.Collections.Generic;
using System.IO;

namespace FizzBuzz
{
    /// W razie pytań proszę pisać na cwirko.krzysztofc@gmail.com
    class Program
    {     
        public static string CurrentPath = AppDomain.CurrentDomain.BaseDirectory;
        public static NestedDirectories CurrentDirectoriesStructure;

        private static void Main(string[] args)
        {
            Start();
        }

        public static void Start()
        {
            Console.Clear();                                                                                                                    
            Console.ForegroundColor = ConsoleColor.White;                                                                                       
            MakeColoredLine(new ColoredString("What you want to do?"), new ColoredString(" Type method number.", ConsoleColor.Magenta));
            MakeColoredLine("1. FizzBuzz", ConsoleColor.Cyan);
            MakeColoredLine("2. DeepDive", ConsoleColor.Green);
            MakeColoredLine("3. DrownItDown", ConsoleColor.Yellow);
            MakeColoredLine("4. Exit", ConsoleColor.Red);
            MakeColoredLine("Author: Krzysztof Cwirko\n", ConsoleColor.DarkYellow);                                          

            var input = GetValueFromUser(1, 4, ConsoleColor.Magenta);

            switch (input)
            {
                case 1:
                    FizzBuzz();
                    break;
                case 2:
                    DeepDive();
                    break;
                case 3:
                    DrownItDown();
                    break;
                case 4:
                    Exit();
                    break;
                default:
                    break;  
            }
        }

        public static void FizzBuzz()
        {
            Console.Clear();
            MakeColoredLine("FizzBuzz", ConsoleColor.Cyan);                                                                                                          
            MakeColoredLine(new ColoredString("If number will be divisible by "), new ColoredString("2", ConsoleColor.Cyan), new ColoredString(", then application will write "),
                new ColoredString("\"Fizz!\"", ConsoleColor.Cyan), new ColoredString("."));
            MakeColoredLine(new ColoredString("If number will be divisible by "), new ColoredString("3", ConsoleColor.Cyan), new ColoredString(", then application will write "),
                new ColoredString("\"Buzz!\"", ConsoleColor.Cyan), new ColoredString("."));
            MakeColoredLine(new ColoredString("If by both, application will write "),
                new ColoredString("\"FizzBuzz!\"", ConsoleColor.Cyan), new ColoredString("."));

            var input = GetValueFromUser(1, 1000, ConsoleColor.Cyan);                           

            switch (input % 2)                                                                  
            {
                case 0 when input % 3 == 0:                                                     
                    MakeColoredLine("FizzBuzz!", ConsoleColor.Cyan);
                    break;
                case 0:
                    MakeColoredLine("Fizz!", ConsoleColor.Cyan);            
                    break;
                default:
                {
                    if (input % 3 != 0)
                    {
                        MakeColoredLine("The number you entered cannot be neither divided by 2 nor 3.",      
                            ConsoleColor.Yellow);
                    }
                    else
                    {
                        MakeColoredLine("Buzz!", ConsoleColor.Cyan);         
                    }

                    break;
                }
            }


            //Asking user to try again, or return to method selection.

            MakeColoredLine(new ColoredString("\nDo you want to try again?"), new ColoredString(" Type y for yes, or n for no.", ConsoleColor.Yellow));

            while (true)
            {
                var decision = Console.ReadLine();

                switch (decision)
                {
                    case "y":
                        FizzBuzz();
                        break;
                    case "n":
                        Start();
                        break;
                    default:
                        IncorrectArgument();
                        continue;
                }

                break;
            }
        }

        public static void DeepDive()
        {
            Console.Clear();

            if (UserDeletedDirectories())       
            {
                MakeColoredLine("You have deleted some of directories. Cleaning up data...", ConsoleColor.Red);

                CurrentDirectoriesStructure = null;
            }

            MakeColoredLine("DeepDive", ConsoleColor.Green);
            Console.WriteLine("Application will create 1-5 nested directories at given path.\nEach next directory will be in previous one.");
            Console.WriteLine("Example: your input is 4, method creates 4 folders like this:");
            Console.WriteLine("(1) aaaa-bbbb-cccc");
            Console.WriteLine("(2)     aaaa-bbbb-cccc");
            Console.WriteLine("(3)         aaaa-bbbb-cccc");
            Console.WriteLine("(4)             aaaa-bbbb-cccc\n");

            if (CurrentDirectoriesStructure != null)
            {
                if (CurrentDirectoriesStructure.Size != 5)  //Asks user to extend structure.
                {
                    MakeColoredLine(
                        new ColoredString(
                            "During this runtime, you have already used this method. Do you want to add more nested directories?",
                            ConsoleColor.Yellow), new ColoredString(" Current size: "),
                        new ColoredString((CurrentDirectoriesStructure.Size).ToString(), ConsoleColor.Green));
                    MakeColoredLine("Type y for yes, or n for no.", ConsoleColor.Yellow);

                    while (true)
                    {
                        var decision = Console.ReadLine();

                        switch (decision)
                        {
                            case "y":
                                break;
                            case "n":
                                Start();
                                break;
                            default:
                                IncorrectArgument();
                                continue;
                        }

                        break;
                    }

                    CreateNewStructure(CurrentDirectoriesStructure.Size,
                        GetValueFromUser(CurrentDirectoriesStructure.Size + 1, 5, ConsoleColor.Green));
                    return;
                }

                //If user made 5 directories - returning to method selection.
                MakeColoredLine(
                    new ColoredString("During this runtime, you have already used this method and created maximum number of directories (5).",
                        ConsoleColor.Yellow),

                    new ColoredString("\nYou should try \"DrownItDown\" method instead.", ConsoleColor.Green));
                Console.ReadKey();
                Start();
            }

            TryToChangePath();                                                          
            CreateNewStructure(0, GetValueFromUser(1, 5, ConsoleColor.Green));         
        }

        public static void ChangePath()
        {
            MakeColoredLine("Please enter each directory separately. For example:\nC:\nusers", ConsoleColor.Yellow);
            MakeColoredLine("If you want to stop, write: \"ex.it\"New path: ", ConsoleColor.Green);

            var names = new List<string>();

            while (true)
            {
                var newName = Console.ReadLine();

                if(Equals(newName, "ex.it"))
                    break;

                var nPath = new string[names.Count + 1];

                for (var i = 0; i < names.Count + 1; i++)
                {
                    if (i != names.Count)
                        nPath[i] = names[i];
                    else
                        nPath[i] = newName;
                }

                try
                {
                    if (!Directory.Exists(Path.Combine(nPath)))
                    {
                        IncorrectArgument();
                        continue;
                    }
                }
                catch
                {
                    IncorrectArgument();
                    continue;
                }

                names.Add(newName);            
            }

            if (names.Count > 0)
            {
                try
                {
                    CurrentPath = Path.Combine(names.ToArray());
                }
                catch
                {
                    MakeColoredLine("Something went wrong. Try again.", ConsoleColor.Red);
                    CurrentPath = AppDomain.CurrentDomain.BaseDirectory;
                    ChangePath();
                }
            }

            MakeColoredLine(new ColoredString("New path: "), new ColoredString(CurrentPath, ConsoleColor.Green));
        }

        public static void TryToChangePath()
        {
            MakeColoredLine(new ColoredString("Default path: "), new ColoredString(CurrentPath, ConsoleColor.Green));
            MakeColoredLine(new ColoredString("\nDo you want change path?"), new ColoredString(" Type y for yes, or n for no.", ConsoleColor.Yellow));

            while (true)
            {
                var pathDecision = Console.ReadLine();

                switch (pathDecision)
                {
                    case "y":
                        ChangePath();
                        break;
                    case "n":
                        break;
                    default:
                        IncorrectArgument();
                        continue;
                }

                break;
            }
        }

        public static void CreateNewStructure(int startingValue, int maxValue)
        {
            var newNestedDirectoriesStructure = CurrentDirectoriesStructure ?? new NestedDirectories(maxValue);

            if (!Directory.Exists(CurrentPath))
            {
                CurrentPath = Directory.GetCurrentDirectory();
                MakeColoredLine("Entered path doesn't exist. Changing path to: " + Directory.GetCurrentDirectory(), ConsoleColor.Red);
            }

            for (var x = startingValue; x < maxValue; x++)  //Creating or adding new directories.
            {
                var newDirectoryName = Guid.NewGuid();

                try
                {
                    if (x != 0)
                    {
                        Directory.CreateDirectory(newNestedDirectoriesStructure.DirectoriesPaths[x - 1] + Path.DirectorySeparatorChar +
                                                  newDirectoryName);
                        newNestedDirectoriesStructure.DirectoriesPaths[x] =
                            newNestedDirectoriesStructure.DirectoriesPaths[x - 1] + Path.DirectorySeparatorChar +
                            newDirectoryName;
                    }
                    else
                    {
                        Directory.CreateDirectory(CurrentPath + Path.DirectorySeparatorChar + newDirectoryName);
                        newNestedDirectoriesStructure.DirectoriesPaths[x] = CurrentPath + Path.DirectorySeparatorChar + newDirectoryName;
                    }
                }
                catch
                {
                    MakeColoredLine("Error, returning to method selection.\nTry running this app as administrator (sudo).", ConsoleColor.Red);
                    Console.ReadKey();
                    Start();
                    return;
                }

                newNestedDirectoriesStructure.FilledWithEmptyFile[x] = false;   //Just in case.
            }

            CurrentDirectoriesStructure = newNestedDirectoriesStructure;
            CurrentDirectoriesStructure.Size = maxValue;

            MakeColoredLine("Completed!", ConsoleColor.Green);
            MakeColoredLine("You should try \"DrownItDown\" method now.", ConsoleColor.Yellow);
            Console.ReadKey();
            Start();        
        }

        public static void DrownItDown()
        {
            Console.Clear();
            MakeColoredLine(new ColoredString("DrownItDown", ConsoleColor.Yellow));

            if (CurrentDirectoriesStructure == null)    
            {
                MakeColoredLine(new ColoredString(
                    "This method requires at least one directory made using "), new ColoredString("\"DeepDive\"", ConsoleColor.Green), new ColoredString(" method. \nIt seems like you haven't tried it yet."));
                MakeColoredLine("Press any key to return to method selection.", ConsoleColor.Yellow);
                Console.ReadKey();
                Start();
                return;
            }

            Console.WriteLine("Application will create an empty file in nested folder which you want.");
            MakeColoredLine(new ColoredString("For example: your input is 2, method creates an empty file in second directory ("), new ColoredString("from DeepDive method", ConsoleColor.Green), new ColoredString(")."));
            Console.WriteLine(".../aaaa-bbbb-cccc/aaaa-bbbb-cccc/empty file will be there.");

            var input = GetValueFromUser(1, CurrentDirectoriesStructure.Size, ConsoleColor.Yellow);

            if (CurrentDirectoriesStructure.FilledWithEmptyFile[input - 1])
            {
                if (File.Exists(CurrentDirectoriesStructure.DirectoriesPaths[input - 1]))
                {
                    if (!AskForOverwrite())
                    {
                        MakeColoredLine("Returning to method selection.", ConsoleColor.Yellow);
                        Console.ReadKey();
                        Start();
                        return;
                    }
                }
                else
                {
                    CurrentDirectoriesStructure.FilledWithEmptyFile[input - 1] = false;
                }
            }

            try
            {
                if (File.Exists(CurrentDirectoriesStructure.DirectoriesPaths[input - 1] + Path.DirectorySeparatorChar + "empty.txt"))
                {
                    try
                    {
                        File.Delete(CurrentDirectoriesStructure.DirectoriesPaths[input - 1] + Path.DirectorySeparatorChar + "empty.txt");
                    }
                    catch
                    {
                        MakeColoredLine("Something went wrong. Try running this application as administrator (sudo). Returning to method selection.",
                            ConsoleColor.Red);

                        Console.ReadKey();
                        Start();
                    }
                }

                var file = File.Create(CurrentDirectoriesStructure.DirectoriesPaths[input - 1] + Path.DirectorySeparatorChar + "empty.txt");
                file.Close();
                CurrentDirectoriesStructure.FilledWithEmptyFile[input - 1] = true;
            }
            catch
            {
                MakeColoredLine("Something went wrong. Try running this application as administrator (sudo). Returning to method selection.",
                    ConsoleColor.Red);
                Console.ReadKey();
                Start();
            }

            MakeColoredLine("Completed! Returning to method selection.", ConsoleColor.Yellow);
            Console.ReadKey();
            Start();
        }

        public static bool AskForOverwrite()
        {
            Console.WriteLine("You have already created file in this directory. Do you want to overwrite this file?");
            MakeColoredLine("Type \"y\" for yes, \"n\" for no.", ConsoleColor.Yellow);

            while (true)
            {
                var decision = Console.ReadLine();

                switch (decision)
                {
                    case "y":
                        return true;
                    case "n":
                        return false;
                    default:
                        IncorrectArgument();
                        continue;
                }
            }
        }

        public static void Exit()
        {
           Console.Clear();
           Environment.Exit(0);
        }

        public static void MakeColoredLine(params ColoredString[] text)
        {
            foreach (var fragment in text)
            {
                Console.ForegroundColor = fragment.Color;
                Console.Write(fragment.Content);
            }

            Console.Write("\n");
            Console.ForegroundColor = ConsoleColor.White;
        }

        public static void MakeColoredLine(string text, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(text);
            //Console.Write("\n");
            Console.ForegroundColor = ConsoleColor.White;
        }

        public static void IncorrectArgument()
        {
            MakeColoredLine("Please enter the correct argument.", ConsoleColor.Red);
        }

        public static bool NumberOutOfRange(int value, int startingNumber, int endingNumber)
        {
            if (value >= startingNumber && value <= endingNumber) return false;

            IncorrectArgument();
            return true;
        }

        public static bool UserDeletedDirectories()
        {
            if (CurrentDirectoriesStructure == null) return false;

            for (var x = 0; x < CurrentDirectoriesStructure.Size; x++)
            {
                if (!Directory.Exists(CurrentDirectoriesStructure.DirectoriesPaths[x])) return true;
            }

            return false;
        }
        
        public static int GetValueFromUser(int startingNumber, int endingNumber, ConsoleColor color)
        {
            MakeColoredLine(new ColoredString("Please enter the number from "), new ColoredString(startingNumber.ToString(), color), new ColoredString(" to "), new ColoredString(endingNumber.ToString(), color), new ColoredString("."));

            var value = 0;

            while (true)
            {
                try
                {
                    value = Convert.ToInt32(Console.ReadLine());
                }
                catch
                {
                    IncorrectArgument();
                    continue;
                }

                if (NumberOutOfRange(value, startingNumber, endingNumber))
                    continue;

                break;
            }

            return value;
        }
    }
}
